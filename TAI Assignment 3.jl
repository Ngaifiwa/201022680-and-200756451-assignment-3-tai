### A Pluto.jl notebook ###
# v0.19.25

using Markdown
using InteractiveUtils

# ╔═╡ 792f0d49-f158-45be-a3d9-906086ad9722
# instaling the package to creat the data set
using Random

# ╔═╡ 4b9de8f7-7f07-4c82-a16d-96862e1e23df
#TAI Assignment 3
# Done by Festus Ngaifiwa Shilombuelua (201022680) and Helvi Nelao Iyambo ( 200756451)

# ╔═╡ 59908b02-c250-48e4-a485-8b26022fc876
# Set random seed for reproducibility
Random.seed!(123)

# ╔═╡ 29f432f3-6856-4a94-82de-62655d796364
# Define the number of courses, students, and career tracks
begin
	num_courses = 20
	num_students = 10
	num_career_tracks = 3
end

# ╔═╡ c9dee4bf-d251-46de-bf6d-337054a08d11
# Define the levels and career tracks
begin
	levels = ["Beginner", "Intermediate", "Advanced"]
	career_tracks = ["Web Development", "Data Science", "Cybersecurity"]
end

# ╔═╡ fa9e3147-5e76-466a-9f26-ff40598226bf
# Generate the course information
begin
	courses = Dict{String, Any}[]
		for i in 1:num_courses
	    course_id = "C" * string(i)
	    course_name = "Course $i"
	    level = levels[rand(1:length(levels))]
	    career_track = career_tracks[rand(1:length(career_tracks))]
	    prerequisite_courses = rand(1:num_courses, rand(0:5))
	    
	    push!(courses, Dict(
	        "Course ID" => course_id,
	        "Course Name" => course_name,
	        "Course Level" => level,
	        "Career Track" => career_track,
	        "Prerequisite Courses" => prerequisite_courses
	    ))
	end
end

# ╔═╡ cedef751-b1fe-4dc4-ab7f-03b8a240939b
# Generate the student profiles
begin
	students = Dict{String, Any}[]
	for i in 1:num_students
	    student_id = "S" * string(i)
	    completed_courses = rand(1:num_courses, rand(0:20))
	    career_track = career_tracks[rand(1:num_career_tracks)]
	    
	    push!(students, Dict(
	        "Student ID" => student_id,
	        "Completed Courses" => completed_courses,
	        "Career Plan/Track" => career_track
	    ))
	end
end

# ╔═╡ 72bdc0f4-9950-4295-8901-90200c00139d
# Recommender system function
function course_recommender(student)
    career_track = student["Career Plan/Track"]
    completed_courses = student["Completed Courses"]
    
    # Filter courses based on career track and completed courses
    recommended_courses = filter(course -> course["Career Track"] == career_track &&
                                           !any(prerequisite -> !(prerequisite in completed_courses),
                                           course["Prerequisite Courses"]),
                                courses)
    
    return recommended_courses
end

# ╔═╡ 57460dda-69e8-4353-94c0-33e093557aa8
# Measure accuracy and recall
for student in students
    recommended_courses = course_recommender(student)
    ground_truth_courses = filter(course -> course["Career Track"] == student["Career Plan/Track"], courses)
    
    num_correct_recommendations = length(intersect(recommended_courses, ground_truth_courses))
    total_recommendations = length(recommended_courses)
    total_relevant_courses = length(ground_truth_courses)
    
    accuracy = num_correct_recommendations / total_recommendations
    recall = num_correct_recommendations / total_relevant_courses

    println("Recommended Courses for Student ", student["Student ID"], ":")
    for course in recommended_courses
        println(" - ", course["Course Name"])
    end
    
    println("Accuracy: ", accuracy)
    println("Recall: ", recall)
    println()
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Random = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.5"
manifest_format = "2.0"
project_hash = "fa3e19418881bf344f5796e1504923a7c80ab1ed"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"
"""

# ╔═╡ Cell order:
# ╠═4b9de8f7-7f07-4c82-a16d-96862e1e23df
# ╠═792f0d49-f158-45be-a3d9-906086ad9722
# ╠═59908b02-c250-48e4-a485-8b26022fc876
# ╠═29f432f3-6856-4a94-82de-62655d796364
# ╠═c9dee4bf-d251-46de-bf6d-337054a08d11
# ╠═fa9e3147-5e76-466a-9f26-ff40598226bf
# ╠═cedef751-b1fe-4dc4-ab7f-03b8a240939b
# ╠═72bdc0f4-9950-4295-8901-90200c00139d
# ╠═57460dda-69e8-4353-94c0-33e093557aa8
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
